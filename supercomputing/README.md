# supercomputing

## Contents
* ```examples/``` - Example mp/mpi code for playing with parallel programs
* ```slides/``` - Slides from the presentation(s)


## Sources
The files here have been adapted and modified from various online sources. 
* <a href="https://curc.readthedocs.io/en/latest/index.html"> Research Computing University of Colorado Boulder </a>
* <a href="https://www.epcc.ed.ac.uk/"> EPCC Edinburgh </a>
* <a href="https://www.surf.nl/en/dutch-national-supercomputer-snellius"> SURF </a>
