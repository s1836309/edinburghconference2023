"""
Basic scatter with numpy arrays

Adapted from courses publically available from SURF www.surf.nl

Author: CE
"""

from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# send buffer only needed by rank 0
sendbuf = None

if rank == 0:
    sendbuf = np.empty([size, 5], dtype="i")
    sendbuf.T[:, :] = range(size)
    print("[0] Scattering data: ", sendbuf)
recvbuf = np.empty(5, dtype="i")
comm.Scatter(sendbuf, recvbuf, root=0)

assert np.allclose(recvbuf, rank)
print("Rank %d received:\n %r" % (rank, recvbuf))
