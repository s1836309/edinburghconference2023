"""
Basic gather with numpy arrays

Adapted from courses publically available from SURF www.surf.nl

Author: CE
"""
from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#send the rank 10 times as a np array
sendbuf = np.zeros(10, dtype='i') + rank

#revieve buffer only used for rank 0
recvbuf = None

if rank == 0:
  recvbuf = np.empty([size, 10], dtype='i')
  
#Gather data
comm.Gather(sendbuf, recvbuf, root=0)

if rank == 0:
  for i in range(size):
    assert np.allclose(recvbuf[i,:], i)
   
  print("[0] Data gathered is a np array of size:",recvbuf.shape, "Full data:", recvbuf)
