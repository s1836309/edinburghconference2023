"""
Basic non-blocking p2p send and receive

Adapted from courses publically available from SURF www.surf.nl

Author: CE
"""
from mpi4py import MPI
import numpy

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size != 2:
    if rank == 0:
        print("[X] Run this example with 2 procs")
    exit()

# initialize data
if rank == 0:
    data = numpy.arange(1000, dtype="i")
elif rank == 1:
    data = numpy.empty(1000, dtype="i")

# measure communication time
start = MPI.Wtime()
if rank == 0:
    req = comm.Isend([data, MPI.INT], dest=1, tag=77)
    req.Wait()
elif rank == 1:
    req = comm.Irecv([data, MPI.INT], source=0, tag=77)
    req.Wait()
end = MPI.Wtime()
elapsed = end - start

print("Rank %d: Elapsed time is %f seconds." % (rank, elapsed))
