# mpi (Message Passing Interface)

## Description
Collection of examples of usage of MPI written in C and python. For an overview of MPI, see ../slides.

## Building
Each example can be built independantly (no need to build mpi4py) or can be built all at once by running:
* Load MPI libraries if not done so already
* To compile, run ```make```

## Compilation Requirements
* Working mpicc compiler
* Tested working on cplab


# Exercises
1. Run each of the examples in helloworld, message_passing and operations and read the source files to get an idea for what is going on.
2. Using the python or C code examples, write an MPI program which
* Broadcasts a (numpy) array of random numbers between 0 and 1 to every process (array size dependant on number processes)
* Creates a new array with elements x^rank for x in the original array
* Sends the new array back to master to calculate the average of all values
3. Using the C code examples, write a 2-process MPI program to simulate ping-pong where:
* For a given number of passes, each of the two processes passes an integer back and forth representing the number of times the message has passed between them.
4. Building on the C or Python code examples
* make an array of random numbers on the master process
* scatter a subset of these random numbers to other processes
* compute the partial average
* gather the partial averages on the root process
* compute the full average on root and broadcast to the other processes
5. Traffic simulation from <a href="https://github.com/EPCCed/archer2-MPI-2022-03-23/blob/master/exercises/road.pdf"> here. </a>
6. Check extension problems in the slides for more!

## Solutions
Solutions will be added after the workshop. An example ping-pong solution can be found <a href="https://github.com/mpitutorial/mpitutorial/blob/gh-pages/tutorials/mpi-send-and-receive/code/ping_pong.c"> here. </a>
