# examples of operations

## Description
Example of a few mpi operations which can be utilised

## Building
* Load MPI libraries if not done so already (module load intel/mpi)
* To compile, run ```make```
* Execute with ```srun -n <numberofprocs> <gather/reduce/scatter>```

## Compilation Requirements
* Working mpicc compiler
* Tested working on phcomputeXXX

### Scripts
* ```gather``` - Sends data (the proc's rank) to rank 0 which gathers data into one array.
* ```scatter``` - Scatters an array of integers between other ranks.
* ```reduce``` - Collects data (in this case an integer set to rank) from all ranks and sums the result.


