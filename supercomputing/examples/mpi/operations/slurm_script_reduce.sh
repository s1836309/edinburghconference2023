#!/bin/bash
#
# Hello world job submission script
#
#SBATCH --job-name=reduce
#SBATCH -ptraining
#SBATCH --time=00:01:00
#SBATCH --ntasks=4
#SBATCH --output=O_%x_%j.out
#
########################################################################

# Launch the compiled C++ MPI code with srun
srun reduce
