#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    int process_Rank, size_Of_Comm;
    int scattered_Data;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size_Of_Comm);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_Rank);

    int distro_Array[size_Of_Comm];
    if (process_Rank==0) // Root fills in array to scatter
    {
      for (int i=0; i<size_Of_Comm; i++) { distro_Array[i] = i; }
      printf("Rank 0 populates an array with rank numbers \n");
    }

    MPI_Scatter(&distro_Array, 1, MPI_INT, &scattered_Data, 1, MPI_INT, 0, MPI_COMM_WORLD);

    printf("[%d] Process has received: %d \n", process_Rank,scattered_Data);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
