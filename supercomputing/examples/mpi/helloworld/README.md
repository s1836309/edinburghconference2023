# helloworld example

## Description
Basic MPI example where each proccess says "I am rank <rank> out of <total>"

## Building
* Load MPI libraries if not done so already (module load intel/mpi)
* To compile, run ```make helloworld```
* Execute with ```srun -n <numberofprocs> helloworld```



## Compilation Requirements
* Working mpicc compiler
* Tested working on phcomputeXXX
