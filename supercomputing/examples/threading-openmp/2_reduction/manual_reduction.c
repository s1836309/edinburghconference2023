/*
  Example implementing reduction manually
  We will sum integers 1...N and check we get the right number!
  Author: Rory Claydon
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define SQR(x) ((x)*(x))
#define CUB(x) ((x)*(x)*(x))

/*
  Waste some CPU time
*/
double stupidExensiveFunction(const long ii)
{
  double val=0.0;
  for ( int jj=0; jj<100; ++jj )
    val+=pow(val+jj,0.134)/cosh(ii/log(ii+sqrt(ii)));
  return sinh(val);
}

/*
  Compute in serial
*/
double checkSum( const long N )
{
  double sum=0; /* This variable may be updated by each thread! */
  for ( long ii=0; ii<N; ++ii )
  {
    sum+=stupidExensiveFunction(ii+1);
  }
  return sum;
}


/*
  Demonstrate reduction using a critical region and shared/private varibles
*/
double criticalReduction( const long N )
{
  double local_sum, total;
  /* each thread gets a copy of local_sum but all can update total */
#pragma omp parallel private(local_sum) shared(total) default(shared)
  {
    local_sum = 0;
    total = 0;

#pragma omp for
    for ( long ii=0; ii<N; ++ii )
    {
      local_sum+=stupidExensiveFunction(ii+1);
    }

    //Create thread safe region. This means only one thread can update at a time
    #pragma omp critical
    {
      //add each threads partial sum to the total sum
      total += local_sum;
    }
  }
  return total;
}

/*
  Demonstrate reduction using a local array and shared/private variables
  This assumes there are less threads than the length of the array
  There is the potential for this method to be slower due to a phenomenon called
  false sharing and is related to the layout in memory of the array and the
  threads wasting bandwidth copying the same data.
  This is a good warning to check you actually get speedup!
*/
double arrayReduction( const long N )
{
  const int M=12;
  double total=0;
  double partials[M]={0};

  /* each thread gets a copy of threadnum but all can update total */
  int threadnum;
#pragma omp parallel for default(none) private(threadnum) shared(N,partials)
  for ( long ii=0; ii<N; ++ii )
  {
    threadnum = omp_get_thread_num();
    partials[threadnum]+=stupidExensiveFunction(ii+1);
  }

  for ( int ii=0; ii<M; ++ii )
    total+=partials[ii];

  return total;
}

int main()
{

  /* initialise timing variables */
  double start;
  double end;

  /* sum random big number */
  const long N { 10000 };

  start = omp_get_wtime();
  double critical_total = criticalReduction(N);
  end = omp_get_wtime();
  double critical_ptime = end - start;
  printf("Parallel (critical) work took %f seconds\n", critical_ptime);

  start = omp_get_wtime();
  double array_total = arrayReduction(N);
  end = omp_get_wtime();
  double array_ptime = end - start;
  printf("Parallel (array) work took %f seconds\n", array_ptime);

  start = omp_get_wtime();
  double check = checkSum(N);
  end = omp_get_wtime();
  double stime = end - start;
  printf("Serial work took %f seconds\n", stime);

  printf("---------------------------------------------------------------\n");
  printf("Speed up from parallisation by critical: %f %% \n",
    stime/critical_ptime*100
  );
  printf("Speed up from parallisation by array: %f %% \n",
    stime/array_ptime*100
  );

  if ( fabs(critical_total-check)<1e-8 && fabs(array_total-check)<1e-8 )
  {
    printf("Test passed!\n");
    return 0;
  }
  else
  {
    printf("Test failed...\n");
    printf("Critical: Expected %f but obtained %f\n",check,critical_total);
    printf("Array: Expected %f but obtained %f\n",check,array_total);
    return 1;
  }
}
