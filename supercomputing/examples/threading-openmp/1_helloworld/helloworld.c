#include <stdio.h>
#include <stdlib.h>
#include <omp.h> // Needed for omp functionality

int main(){
  // Launch parallel section
#pragma omp parallel
    {
      // Only let one thread greet me at a time
#pragma omp critical
     {
      printf("hello from thread %d\n",omp_get_thread_num());
     }
    }
}
