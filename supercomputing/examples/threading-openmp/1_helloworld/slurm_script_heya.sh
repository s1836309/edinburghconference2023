#!/bin/bash
#
# Hello world job submission script
#
#SBATCH --job-name=heya
#SBATCH -ptraining
#SBATCH --time=00:01:00
#SBATCH --cpus-per-task=4
#SBATCH --output=O_%x_%j.out
#
########################################################################

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Launch the compiled C++ MPI code with srun
srun helloworld.out
