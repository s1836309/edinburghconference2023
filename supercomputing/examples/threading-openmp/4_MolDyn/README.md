# Molecular Dynamics Example

Well done for making it this far! In this example, we let you loose on a larger,
more complex piece of code.

The first thing to do is identify the most expensive part of the code and focus
your efforts on parallelising this. Try timing sections of code with ```omp_get_wtime``` or profiling if you know how.

Make sure before you start to save a copy of the original code and example output so you can check you didn't break anything when you implement parallelism.

We now try two things in the function you identified as the main culprit:
1. Parallelise the outermost loop, choosing a suitable loop schedule
1. Play with using atomics and locks to try to improve performance
1. Try to use a reduction array to avoid using the atomics
1. Challenge: Try to move the parallel region to before the main (watch out for reduction variable initialisations!)

As usual, you can compile with ```make```, clean with ```make clean``` and run
using ```./md.out```.

You will need to set the number of threads by writing ```OMP_NUM_THREADS=N``` in terminal where ```N``` is an integer equal to the number of threads you want. For this example we will use the following environment variables.

Locally you can set
* ```export OMP_NUM_THREADS=N```      # Set thread number
* ```export OMP_WAIT_POLICY=active``` # Reduce latency in resuming work
* ```export OMP_DYNAMIC=false```      # Get the number of threads you asked for
* ```export OMP_PROC_BIND=true```     # Prevent threads moving cores

This example is from [EPCC](https://www.archer2.ac.uk/training/courses/220830-openmp/).
