#!/bin/bash
#
# Hello world job submission script
#
#SBATCH --job-name=md
#SBATCH -ptraining
#SBATCH --time=00:01:00
#SBATCH --cpus-per-task=4
#SBATCH --output=O_%x_%j.out
#
########################################################################

# Set number of threads
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Reduce time taken to reuse thread
export OMP_WAIT_POLICY=active

# Disallow dynamically changing the number of threads we request
export OMP_DYNAMIC=false

# Try to bind threads to cpu - try out values of spread or close too
export OMP_PROC_BIND=true

# turns on display of OMP's internal control variables
export OMP_DISPLAY_ENV=true

# display the affinity of each OMP thread
export OMP_DISPLAY_AFFINITY=true

# controls the format of the thread affinityexport
export OMP_AFFINITY_FORMAT="Thread Affinity: %0.3L %.8n %.15{thread_affinity} %.12H"

# control where the OpenMP threads will run - try sockets, numa_domains, ll_caches
export OMP_PLACES=cores


# Launch the compiled C++ MPI code with srun (try to avoid multithreading)
srun --hint=nomultithread md.out
