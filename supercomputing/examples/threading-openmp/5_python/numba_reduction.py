"""
    Introduction to numba
    This shows how to make a simple parallel loop and compute a reduction

"""
import sys

import numba
from numba import njit, prange, set_num_threads
import numpy as np
import time


@njit(parallel=True)
def two_d_array_reduction_prod(n):
    shp = (13, 17)
    result1 = 2 * np.ones(shp, np.int_)
    tmp = 2 * np.ones_like(result1)

    for i in prange(n):
        result1 *= tmp

    return result1


if __name__ == "__main__":

    set_num_threads(int(sys.argv[1]))

    start = time.perf_counter()
    res = two_d_array_reduction_prod(1000000000)
    end = time.perf_counter()
    print(f"Took {end-start} seconds")
    # print(two_d_array_reduction_prod.parallel_diagnostics(level=4))
