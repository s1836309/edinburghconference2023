"""
    This script gives an example of more general threading, not necessarily only compatible with numpy and native types
    as with numba

    Please do not write code like this, I just ran out of time
"""

# Standard modules
from math import sqrt
import time

# Third part modules
from joblib import Parallel, delayed


def expensive_function():
    time.sleep(0.1)


if __name__ == "__main__":
    num_runs = 100
    print("serial")
    start = time.perf_counter()
    res = [expensive_function() for _ in range(num_runs)]
    end = time.perf_counter()
    print(f"Took {end - start} s")

    print("parallel")
    start = time.perf_counter()
    res = Parallel(n_jobs=6, verbose=0, prefer="threads")(delayed(expensive_function)() for _ in range(num_runs))
    end = time.perf_counter()
    print(f"Took {end - start} s")
