# Standard modules
import os
import sys
from glob import glob
import numpy as np
import pandas as pd

# Custom modules
import utilities as ut


def read_cell_data(filename):
    df = pd.read_csv(filename, sep='\t')
    df['radius'] = df['radius'].apply(lambda x: 2 * x)
    return df[['pos_x', 'pos_y', 'ori_x', 'ori_y', 'length', 'radius']].to_numpy()


def save_data(run_dir, file, dr, grid, density):
    sv_dir_spef = os.path.basename(file).strip('.dat')
    sv_dir = f"data/{run_dir}/output_data/{sv_dir_spef}"
    os.makedirs(sv_dir, exist_ok=True)
    np.savetxt(f"{sv_dir}/grid_x_{dr}.dat", grid[0])
    np.savetxt(f"{sv_dir}/grid_y_{dr}.dat", grid[1])
    np.savetxt(f"{sv_dir}/density_{dr}.dat", density)


def plot_bacteria_density_grid(run_dir, dr=0.5, threads=6):
    for file in glob(f"data/{run_dir}/*.dat"):
        print(f"Finding density of cells in file: {file}")
        cell_data = read_cell_data(f"{file}")
        density, grid = ut.calc_density(cell_data, dr=dr, threads=threads)
        save_data(run_dir, file, dr, grid, density)
        ut.plot_field(density, grid, cell_data)


if __name__ == "__main__":
    # This should use argparse and include exceptions to handle data, but I ran out of time
    plot_bacteria_density_grid(run_dir=sys.argv[1], dr=float(sys.argv[2]), threads=int(sys.argv[3]))
