# Standard modules
import numpy as np
from numba import njit, prange


@njit
def get_smooth_cell_rho_nat(cell_data, r, cut_off):
    """
        Get a smoothed density profile for this cell using native types

        This will only work in 2D

        Parameters:
            :param r:
            :param cell_data:
            :param cut_off:
            cell_data:
                stuff
            r: array, float
                position at which the profile is observed
            sigma: float
                smoothing length
        Returns:
            rho: float
                density at this location
    """
    sigma = 1
    rcm = cell_data[0:2]
    tang = cell_data[2:4]
    length = cell_data[4]
    diameter = cell_data[5]

    delta_r = r - rcm
    if np.linalg.norm(delta_r) > cut_off:
        return 0

    perp = np.array([tang[1], -tang[0]])
    perp /= np.linalg.norm(perp)

    eff_hlf_L = 0.5 * (length + diameter)  # effective half-length
    eff_hlf_W = 0.5 * diameter  # effective half width

    smooth_main = 0.5 * (np.tanh((tang.dot(delta_r) + eff_hlf_L) / sigma)
                         - np.tanh((tang.dot(delta_r) - eff_hlf_L) / sigma)
                         )
    smooth_perp = 0.5 * (np.tanh((perp.dot(delta_r) + eff_hlf_W) / sigma)
                         - np.tanh((perp.dot(delta_r) - eff_hlf_W) / sigma)
                         )
    return smooth_main * smooth_perp


@njit(parallel=True)
def find_density_grid(result, cell_data, density_grid, grid_coords, cut_off):
    n, m = result.shape
    for ii in prange(n):
        for jj in range(m):
            if result[ii, jj] >= 0:
                pnt = result[ii, jj]
                density_grid[pnt] += get_smooth_cell_rho_nat(cell_data[ii],
                                                             grid_coords[pnt],
                                                             cut_off
                                                             )
