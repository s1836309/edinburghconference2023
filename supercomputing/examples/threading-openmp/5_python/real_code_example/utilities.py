
# Standard modules
import numpy as np
from scipy.spatial.ckdtree import cKDTree
from numba import set_num_threads
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.font_manager as fm
fontprops = fm.FontProperties(size=10)
import time

# Custom modules
import parallel_section as ps


def plot_field(field, grid, cell_data):
    field[field < 1e-5] = np.nan
    fig, ax = plt.subplots(1, 1, figsize=[10, 10])
    ax.axis("scaled")
    cs = ax.contourf(*grid, field, alpha=0.8,
                     cmap='inferno', zorder=2)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(cs, cax=cax)
    cbar.set_label(r"$\rho$", size=14)
    cbar.ax.tick_params(labelsize=12)
    ax.set_xlim(cell_data[:, 0].min(), cell_data[:, 0].max())
    ax.set_ylim(cell_data[:, 1].min(), cell_data[:, 1].max())
    plt.show()

def create_grid(cell_data, dr):
    """
    Create a coarse grid to represent the density on
    :return:
    """

    # Add on increase by the maximum half-length to include ends of cells
    min_x, min_y = np.min(cell_data[:, :2], axis=0) - 3.5
    max_x, max_y = np.max(cell_data[:, :2], axis=0) + 3.5

    # Ensure all cells are enclosed
    x = np.arange(min_x, max_x + dr, dr)
    y = np.arange(min_y, max_y + dr, dr)

    xv, yv = np.meshgrid(x, y, sparse=False, indexing='xy')

    return xv, yv


def get_cell_grid_neighbours(grid_coords, cell_data, cut_off):
    """
        Bin the grid points to the nearest cell locations
    """

    grid_tree = cKDTree(grid_coords)

    # bin cells to grid points they are withing cut_off of
    xs = cell_data[:, :2]
    cell_tree = cKDTree(xs)
    result = cell_tree.query_ball_tree(grid_tree, r=cut_off)

    # create a lookup table of cell indices to map onto each grid point
    max_neighbours = max([len(res) for res in result])
    nat_result = -np.ones((len(result), max_neighbours), dtype=int)
    for ii, nlist in enumerate(result):
        for jj, nn in enumerate(nlist):
            nat_result[ii, jj] = nlist[jj]

    return nat_result


def calc_density(cell_data, dr=1, cut_off=6, threads=6):
    grid = create_grid(cell_data, dr)
    xv, yv = grid

    # grid to list of coordinates
    grid_coords = np.array([xv.flatten(), yv.flatten()]).T

    # find cell neighbours on grid
    nat_result = get_cell_grid_neighbours(grid_coords, cell_data, cut_off=cut_off)

    # find density on grid
    # Column and row element numbers
    ny, nx = xv.shape
    density_grid = np.zeros(ny * nx)

    print(f"Launch {threads} threads")
    start = time.perf_counter()
    set_num_threads(threads)
    ps.find_density_grid(nat_result, cell_data, density_grid, grid_coords, cut_off)
    end = time.perf_counter()
    print(f"Took {end-start} seconds")
    density_grid = density_grid.reshape((ny, nx))

    return density_grid, grid
