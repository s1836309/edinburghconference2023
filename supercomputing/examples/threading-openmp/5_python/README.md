# Python Threading Examples

Well done for making it this far! This is very much an extension, and consists of a few python examples.

Firstly, set up the environment:
```
conda env create -f supercomputing.yml
conda activate supercomputing
```
You should now be able to run the exercise.

We show a simple example of shared memory threading in Python in ```numba_reduction.py```.

To run:
```python numba_reduction.py num_threads```

**Exercise:** Try changing the number of threads launched and the size of the system. When do you get a good speed up?

Numba is amazing, but needs native types or numba. What if we want to parallelise over plotting figures? ```joblib``` allows you to thread over general functions with non-native and custom types. See ```joblib_embarrassing.py``` for an example.

To run:
```python joblib_embarrassing.py```

**Exercise:** Try changing the expensive function to something more interesting. When do you get good speed up with different thread numbers and system size? 


In the ```real_code_example``` directory, we give an example of using shared memory parallelisation in a snippet of code from a PhD project. Numba is used to speed up calculating a field. 

To run:
```python main.py run_dir dr threads```
where run_dir=large or small, dr=1 is a good start and threads=1-8

**Exercise:** Add a joblib function to plot the outputs.

**Exercise:**  Play with the number of cores used and the system sizes and see how this affects the time taken to finish the simulations!

If you have time, try ```numba```, ```joblib``` or ```multiprocessing``` in some of your own work!
