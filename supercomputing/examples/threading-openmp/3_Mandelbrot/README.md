# Mandlebrot example

In this example we let you loose on parallelising a more interesting code!
We have so far seen how to parallelise loops and learnt something about declaring
how data should be shared between threads. We now build on that by exploring
nested parallisation and scheduling. This example is taken from EPCC.

Open ```area.c```. This is the code we aim to parallelise. Answers are in ```par_area.c```.
Before you begin, make a note of the answer you get from the serial version of ```area.c```, and of its execution time.

Tasks (in all cases check you get the same answer for any number of threads):
1. Parallelise the outermost loop. Make sure to add the default(none) option and
think about how the data should be shared.  
1. Try to implement a collapse clause!
1. Add a scheduling clause and experiment with different options. What is the
speed up you can achieve?
1. Now try nested parallisation. For this, we will need to first write the
following in terminal: ```export OMP_NESTED=true``` to enable this and then
```export OMP_NUM_THREADS=2,4``` which set the outer threads to 2 and inner
threads to 4. Make sure the product does not exceed your available cores!
Experiment with nested parallisation. Does this work better than your original
efforts in parallising only the outer loop?

This example is from [EPCC](https://www.archer2.ac.uk/training/courses/220830-openmp/)
