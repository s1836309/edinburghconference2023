# Machine Learning

## Link to workshops:
* <a href="https://colab.research.google.com/github/harry-rendell/MLworkshop/blob/main/notebooks/workshop_1.ipynb"> Workshop 1 - Introduction to Machine Learning </a>
* <a href="https://colab.research.google.com/github/harry-rendell/MLworkshop/blob/main/notebooks/workshop_2.ipynb"> Workshop 2 - Advanced Machine Learning </a>

Note: you will need a google account to be able to run these notebooks. 
If you would rather run locally, then download the mlworkshop.yml file and use
	```conda env create -f mlworkshop.yml```
to create a conda environment with the required packages. Then download the notebooks and launch them while in this environment.

## Contents
* ```notebooks/``` - workshop notebooks
* ```notebook_solns/``` - Solutions to workshop notebooks
* ```slides/``` - Slides for the presentation(s)
