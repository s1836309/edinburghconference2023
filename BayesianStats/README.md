# Bayesian Statistics

## Link to exercises:
* <a href="https://colab.research.google.com/drive/1TfSKoLGukj49xXmqvW3LEVSCR6VLJxuI?usp=sharing"> Exercise 1 - Playing around with simple priors and posteriors </a>
* <a href="https://colab.research.google.com/drive/1kYN_oDanaGrtKNmjCg5F17ctWj8u3xRJ?usp=sharing"> Exercise 2 - A model of sexual dimorphism with MCMC </a>

Note: you will need a google account to be able to run these notebooks. 
Note 2: you need to download the mixture_data.txt file from this repository and upload it to the second notebook in order to do the exercise.

Solution to the second exercise can be found here:
* <a href="https://colab.research.google.com/drive/1L3oHh-mMl8-1Ur75YIAgLp_GRW1FGOWW?usp=sharing"> Exercise 2 solution </a>

Here's an amazing online course on Bayesian Computation that I used as basis for this workshop:
* <a href="https://dataorigami.net/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers"> Probabilistic Programming and Bayesian Methods for Hackers </a>
